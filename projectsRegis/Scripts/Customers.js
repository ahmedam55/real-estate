﻿var app = angular.module('application', ['ui.bootstrap']);

// declare a new module, and inject the $compileProvider
 app.directive('compile', function($compile) {
    // directive factory creates a link function
    return function(scope, element, attrs) {
      scope.$watch(
        function(scope) {
           // watch the 'compile' expression for changes
          return scope.$eval(attrs.compile);
        },
        function(value) {
          // when the 'compile' expression changes
          // assign it into the current DOM
          element.html(value);

          // compile the new DOM and link it to the current
          // scope.
          // NOTE: we only compile .childNodes so that
          // we don't get into infinite loop compiling ourselves
          $compile(element.contents())(scope);
        }
      );
    };
  })


app.directive('editable', function() {
    var editTemplate = '<input type="text" ng-model="editableValue" ng-show="inEditMode" class="enterToSpan" >';
    var viewTemplate = '<span ng-hide="inEditMode"  ng-click="switchToEditMode($event)">{{ editableValue }}</span>';
    return {
        restrict: 'E',
        scope: {
            editableValue: '=ngModel',
            bindAttr: '='
        },
        replace: true,
        compile: function(tElement, tAttrs, transclude) {
            var viewElement = angular.element(viewTemplate);
            tElement.html(editTemplate);
            tElement.append(viewElement);

            

            return function (scope, element, attrs) {
                $('.enterToSpan').keypress(function (e) {
                    if (e.which == 13) {
                        scope.switchToViewMode();
                        scope.$apply();
                    }
                })
                scope.inEditMode = false;

                scope.switchToViewMode = function() {
                    scope.inEditMode = false;
                }
                scope.switchToEditMode = function (e) {
                    var elem = $(angular.element(e.toElement)).parent().children().eq(0);
                    setTimeout(function(){elem.focus();},10)
                   /// $(angular.element(e.srcElement)).fadeOut();
                    //alert();
                    //var elem = $('.enterToSpan');
                    //elem.each(function () {
                    //    if (elem.is(':visible')) {
                            
                    //        $(this).focus();
                    //    }
                    //});
                    //elem.eq(0).focus();
                    scope.inEditMode = true;
                }
            }
        }
    }
})

var genericSearch = function (items, keyProp, key) {



    var arrayToReturn = [];
    for (var i = 0; i < items.length; i++) {
        // console.log("input arrary:"+items+" input id:"+id);
        if (items[i][keyProp] == key) {
            arrayToReturn.push(items[i]);
        }
    }
    //console.log("output array:"+ arrayToReturn);
    return arrayToReturn;
};
app.filter('keyequal', function () {
    return function (items, keyProp, key) {
        return genericSearch(items, keyProp, key);       
    };
});

function zerothdate(input) {
    if (input == 0) { input++ };
    return (input > 9) ? input : ('0' + input)

}
function dateFormat(elem) {
    if (elem.value.indexOf('T') == -1) {
      
        elem.value += 'T' + zerothdate(new Date().getHours()) + ':' + zerothdate(new Date().getMinutes()) + ':' +zerothdate(new Date().getSeconds());
     //   console.log($(elem).attr('ng-model').split('.')[0])
        //console.log(scope[$(elem).attr('ng-model')]);
       // console.log($(elem).val());
        scope[$(elem).attr('ng-model').split('.')[0]][$(elem).attr('ng-model').split('.')[1]] = $(elem).val();
        scope.$apply();
    }
}

$(function () {

    $("[onchange]").datepicker({ dateFormat: "yy-mm-dd" });
});


var scope;
function CustomerCtrl($scope, $compile,$timeout) {
    scope = $scope;

    $(function () { alertify.set({ delay: 1000 }); })
  
    setTimeout($('#searchCustomerName').focus(), 1);
    //editable grid (labels on click became input editable on enter return to original :) الحمدلله)
    $scope.ReCustInstallment = {}; 
    $scope.changeme = function (eventy, model) {
        $scope.ReCustInstallment = model;
        var elem = $(angular.element(eventy.toElement));
        console.log(elem)
        var html = '<input class="replace" ng-model="' + elem.attr('ng-model') + '"/>';
        var e = $compile(html)($scope);
        elem.replaceWith(e);
        $('.replace').keypress(function (e) {
            if (e.which == 13) {
                var elem = $(angular.element(this));
                var html = '<label ng-model="' + elem.attr('ng-model') + '" ng-bind="' + elem.attr('ng-model') + '" ng-click="changeme($event)" style="text-align:center">' + $scope[elem.attr('ng-model')] + '</label>';
                var e = $compile(html)($scope);
                elem.replaceWith(e)
            }
        })
    }

    $scope.ReCustBuildingEqual = function (unit) {
        $scope.ReCustBuilding = unit;      
       
        $scope.$apply();
        $("[onchange]").datepicker({ dateFormat: "yy-mm-dd" });
    }

    $scope.showCustomerData = true;

    // $scope.states = [{n:'wwwww',w:123}, {n:'eeeeee',w:321}];
    //['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];




    //putting string in variable to make it'ss manupilation easy 
    $scope.Title = "تسجيل العملاء";

    //creating a cust record
    $scope.create = function () {
        //var newinstallment = {
        //    UnitsID: 329,
        //    DateInstallment: new Date(2013, 12, 30),
        //    value: 45555,
        //    datePayed: new Date(2013, 12, 30),
        //    payed: true,
        //    InstallmentNO: 2
        //}
        //var newbuild = {
        //    UnitsID: 329,
        //    PayedTYpe: 1,
        //    Price: 2000000,
        //    NoInstallment: 12,
        //    FirstInstallment: 12000,
        //    FirstDateInstallment: new Date(2013, 12, 30),
        //    payedAmount: 12000,
        //    DateContract: new Date().getDate()
        //}
        //var other_build = JSON.parse(JSON.stringify(newbuild));
        //other_build.UnitsID = 331;

        //var newcust = {
        //    Name: 'ahmed',
        //    PersonalNO: 2123123213,
        //    Address: 'eeeeeeeee',
        //    TelNO: 134241245,
        //    Notes: '123123123123',
        //    ReCustBuilding: [newbuild, other_build],
        //    ReCustInstallment: [newinstallment, newinstallment],
        //}
        // var object = newcust;
        // console.log(object);
        $.ajax({
            type: 'POST',
            url: '/Customer/create',
            data: $.toDictionary($scope.Customer),
            success: function (id) {

                console.log("hi" +id)
                $scope.read(id);
                $scope.ReCustomers.push({CustomerID:id,Name:$scope.Customer.Name});
                $scope.getunitsNameID();
                $scope.$apply();
                alertify.success("تمت إضافه العميل بنجاح :)");
            },
        })
        //$.post('', $.toDictionary(newcust)).done();

    }
    // $scope.create();

   

    $scope.update = function () {
        //var newinstallment = {
        //    UnitsID: 329,
        //    DateInstallment: new Date(2013, 12, 30),
        //    value: 45555,
        //    datePayed: new Date(2013, 12, 30),
        //    payed: true,
        //    InstallmentNO: 2
        //}
        //var newbuild = {
        //    UnitsID: 329,
        //    PayedTYpe: 1,
        //    Price: 2000000,
        //    NoInstallment: 12,
        //    FirstInstallment: 12000,
        //    FirstDateInstallment: new Date(2013, 12, 30),
        //    payedAmount: 12000,
        //    DateContract: new Date().getDate()
        //}
        //var other_build = JSON.parse(JSON.stringify(newbuild));
        //other_build.UnitsID = 331;

        //var newcust = {
        //    CustomerID: 115,
        //    Name: 'a',
        //    PersonalNO: 2123123213,
        //    Address: 'eeeeeeeee',
        //    TelNO: 134241245,
        //    Notes: '123123123123',
        //    ReCustBuilding: [newbuild, other_build],
        //    ReCustInstallment: [newinstallment, newinstallment],
        //}
        // var object = newcust;
        // console.log(object);

        $scope.Customer.ReCustBuilding.forEach(function (value, i) {

            $scope.Customer.ReCustBuilding[i].ReUnits = null;

        })
        if ($scope.Customer.CustomerID) {
         


            $.ajax({
                type: 'POST',
                url: '/Customer/update',
                data: $.toDictionary($scope.Customer),
                success: function (data) {
                    $scope.read($scope.Customer.CustomerID);
                    console.log("hi" + data)
                    $scope.getunitsNameID();
                      alertify.success("تم تعديل العميل بنجاح :)");
                },

            })
            //$.post('', $.toDictionary(newcust)).done();
        }
        else {
            $scope.create();
          
        }
        $scope.ReCustBuilding = {};
        $scope.ReCustInstallment = {};
        $scope.$apply();
    }
    // $scope.update();



    $scope.$watch('ReCustBuilding.ReUnits.UnitsID', function () {
        $scope.ReCustBuilding.UnitsID = $scope.ReCustBuilding.ReUnits.UnitsID
        $scope.filteredInstallments.forEach(function (value, i) { $scope.filteredInstallments[i].UnitsID = $scope.ReCustBuilding.ReUnits.UnitsID; })
    })



    $scope.delete = function (id) {

        $.ajax({
            type: 'POST',
            url: '/Customer/delete/' + id,
            success: function (data) {
             //   var custArr=$scope.ReCustomers;
                scope.ReCustomers.splice(scope.ReCustomers.indexOf(genericSearch(scope.ReCustomers, 'CustomerID', id)[0]), 1);
                $scope.Customer = {};
                $scope.getunitsNameID();
                $scope.$apply();
                console.log("hi" + data)
                alertify.success("تم حذف العميل بنجاح :)");
            },
        })
        //$.post('', $.toDictionary(newcust)).done();

    }
    //   $scope.delete(118);



    //to get details of a customer
    $scope.Customer = {};
    $scope.read = function (id) {
        if ((!isNaN(id)) && (id != '')) {
            $scope.loading = true;
            $.ajax({
                type: 'POST',
                url: '/Customer/read/' + id,
                success: function (data) {
                  
                    $scope.loading = false;
                    $scope.Customer = data;
                   // alertify.success($scope.Custmer.Name);
                    $scope.$apply();

                },
            })
        }

    }
    //$scope.read(113);




    //to get Names and ids of a customerS
    $scope.selected = undefined;
    $scope.ReCustomers = [];
    $scope.getNames = function () {
        $.ajax({
            type: 'POST',
            url: '/Customer/getNames/',
            success: function (data) {
                $scope.ReCustomers = data;
                $scope.$apply();


            },
        })

    }
    $scope.getNames();


    // Unit/getProjectNameID
    $scope.ProjectNameID = [];
    $scope.getProjectNameID = function () {
        $.ajax({
            type: 'POST',
            url: '/Unit/getProjectNameID/',
            success: function (data) {
                $scope.ProjectNameID = data;
                $scope.$apply();

            },
        })
    }
      $scope.getProjectNameID();

    // Unit/getbuildingNameID
    $scope.buildingNameID = [];
    $scope.getbuildingNameID = function () {
        $.ajax({
            type: 'POST',
            url: '/Unit/getbuildingNameID/',
            success: function (data) {
                $scope.buildingNameID = data;
                $scope.$apply();

            },
        })
    }

     $scope.getbuildingNameID();




    // Unit/deletsdata
     $scope.unitsNameID = [];
     $scope.getunitsNameID = function () {
         $.ajax({
             type: 'POST',
             url: '/Customer/getNamesUnits/',
             success: function (data) {
                 $scope.unitsNameID = data;
                 $scope.$apply();

             },
         })
     }

     $scope.getunitsNameID();


    //method to edit the installments base on the price and payed type and installmentNO  installment period finally date of first one 

     $scope.ChangeInstallment = function () {
         
         $scope.Customer.ReCustInstallment = $scope.Customer.ReCustInstallment || [];
             var Installments = genericSearch($scope.Customer.ReCustInstallment, 'UnitsID', $scope.ReCustBuilding.UnitsID);
             var installmentValue = ($scope.ReCustBuilding.Price / $scope.ReCustBuilding.NoInstallment);

             var newInstallments = [];
            
             var datefirst= $scope.ReCustBuilding.FirstDateInstallment.substring(0,10).split('-');
             var dateOfPay= new Date(parseInt(datefirst[0]),parseInt(datefirst[1])-1,parseInt(datefirst[2]));
             for (var i = 0; i < $scope.ReCustBuilding.NoInstallment; i++) {
                 if(i!=0)
                 date=dateOfPay.setMonth(dateOfPay.getMonth()+parseInt(($scope.ReCustBuilding.PeriodInstallment)));

                 newInstallments.push({ UnitsID: $scope.ReCustBuilding.UnitsID, value: installmentValue.toFixed(2), DateInstallment: zerothdate(dateOfPay.getFullYear()) + "-" + zerothdate((dateOfPay.getMonth() + 1)) + "-" + zerothdate(dateOfPay.getDay())+"T00:00:00", Payed: false, InstallmentNO: (i + 1) });
                 

             }
             for (var i = 0; i < Installments.length; i++) {
                 $scope.Customer.ReCustInstallment.splice($scope.Customer.ReCustInstallment.indexOf(Installments[i]), 1);
             }
             for (var i = 0; i < newInstallments.length; i++) {
                 $scope.Customer.ReCustInstallment.push(newInstallments[i]);
             }
            
             $scope.$apply();
             $("[onchange]").datepicker({ dateFormat: "yy-mm-dd" });
        
     }
     $scope.ChangeUnit = function (id) {
         var item=genericSearch($scope.unitsNameID, 'UnitsID', id);
         $scope.ReCustBuilding.ReUnits.FloorNO = item[0].FloorNO;
         $scope.ReCustBuilding.ReUnits.ModelName = item[0].ModelName;
         $scope.ReCustBuilding.ReUnits.ModelNO = item[0].ModelNO;
         $scope.$apply();
        

     }

    //to validate that all installments = the price of the unit
     $scope.validateTotalInstallemnts = function () {
         $scope.Customer.ReCustInstallment = $scope.Customer.ReCustInstallment || [];
         var Installments = genericSearch($scope.Customer.ReCustInstallment, 'UnitsID', $scope.ReCustBuilding.UnitsID);
        
       
         var totalValue=0;
         Installments.forEach(function (inst, index) {

             totalValue+=parseFloat(inst.value);
         })
         if (Math.round($scope.ReCustBuilding.Price) !== Math.round(totalValue)) {
         
             $scope.totalInstValid = true;
             return

         }


          $scope.totalInstValid = false;

     }




     $scope.addUnit = function () {
         if (!$scope.Customer.ReCustBuilding)
             $scope.Customer.ReCustBuilding=[];
         var x=$scope.Customer.ReCustBuilding;
         x.push({
             "CustBuildingID":'',
             "CustomerID":'',
             "UnitsID":'',
             "PayedTYpe":'',
             "Price":'',
             "NoInstallment":'',
             "FirstInstallment":'',
             "PeriodInstallment":'',
             "FirstDateInstallment":'',
             "payedAmount":'',
             "DateContract": '',
             

             "ReUnits": {
                 "BuildingID": 0,
                 "ProjectID": 0,
                 "UnitNO": 0,
                 "UnitsID": 0,
                 "FloorNO": 0,
                 "ModelName": null,
                 "ModelNO": 0,
                 "Name": ""
             }
         });
         $scope.ReCustBuilding = x[x.length - 1];
         $scope.$apply();


     }

    




}





//model data in JS 
function ReCustInstallment() {
    this.InstallmentID = '';
    this.CustomerID = '';
    this.UnitsID = '';
    this.DateInstallment = '';
    this.value = '';
    this.datePayed = '';
    this.Payed = '';
    this.InstallmentNO = '';

    this.ReCustomers = {};
    this.ReUnits = {}
};

function ReCustBuilding() {
    this.CustBuildingID = '';
    this.CustomerID = '';
    this.UnitsID = '';
    this.PayedTYpe = '';
    this.Price = '';
    this.NoInstallment = '';
    this.PeriodInstallment = '';
    this.FirstInstallment = '';
    this.FirstDateInstallment = '';
    this.payedAmount = '';
    this.partnerID = '';
    this.DateContract = '';

    this.ReCompartner = '';
    this.ReCustomers = '';
    this.ReUnits = ''
};

function ReCustomers() {
    this.CustomerID = '';
    this.Name = '';
    this.PersonalNO = '';
    this.Address = '';
    this.TelNO = '';
    this.MobileNO = '';
    this.Notes = '';
    this.ReCustBuilding = '';
    this.ReCustInstallment = '';
    this.RePayments = '';
};


