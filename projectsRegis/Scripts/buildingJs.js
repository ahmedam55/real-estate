﻿var app = angular.module('app', []);
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.
        when('/units', { templateUrl: '/Home/unitstable', controller: BuildingCtrl }).
        when('/units/:UnitsID', { templateUrl: '/Home/unitsform', controller: BuildingCtrl }).
        otherwise({ redirectTo: '/units' });
}]);
//code for paginator with search
app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
//to make clicking on check box not like clicking on rest of row
function falsy(e) { e.preventDefault(); }
//end
app.filter('checkmark', function () {
    return function (input) {
        return (input == "true") ? '\u2713' : '\u2718';
    };
});

app.filter('idequal', function(){
    
    return function(items, id){
        
        var arrayToReturn = [];        
        for (var i = 0; i < items.length; i++) {
            console.log("input arrary:"+items+" input id:"+id);
            if (items[i].ProjectID == id) {
                arrayToReturn.push(items[i]);
            }
        }
        console.log("output array:"+ arrayToReturn);
        return arrayToReturn;
    };
});

var storedarray = [];

function BuildingCtrl($scope, $routeParams,$timeout) {
    angular.element(document).ready(function () {

   
        $scope.form = [];
      
    
       

            //method to delete by group 
            //as it take any checked buildings and put them in an array                 
            $scope.Unitsstobeodeleted = [];
            $scope.deleteGroup = function (building, event) {


                if (event.target.checked == true) {
                    $scope.Unitsstobeodeleted.push(building);
                    console.log($scope.Unitsstobeodeleted);
                }
                else {
                    $scope.Unitsstobeodeleted.splice($scope.Unitsstobeodeleted.indexOf(building), 1);
                    console.log($scope.Unitsstobeodeleted);
                }
               console.log(event.target.checked);
            }
        //end of method


            var successDeleting = false;
        //method to delete the array carriying buildings to be deleted
            $scope.deleteall = function () {
                for (var i = 0; i < $scope.Unitsstobeodeleted.length; i++) {
                    $.ajax({
                        async:false,
                        url: "/Unit/delete",
                        type: "POST",
                        data: { UnitsID: $scope.Unitsstobeodeleted[i].UnitsID }
     , success: function (data) {
         // console.log(data);
         if (data == true) {
             successDeleting = true;
             
             $scope.form = [];
             $scope.ReUnits.splice($scope.ReUnits.indexOf($scope.searcharray($scope.ReUnits, 'UnitsID', $scope.Unitsstobeodeleted[i].UnitsID)), 1);
             $scope.$apply();
             storedarray = $scope.ReUnits;
            // console.log("deleted id :" + $.grep($scope.ReUnits, function (e) { return e.UnitsID == $routeParams.UnitsID; }))
         }
         else {

             alertify.error("حدث خطأ ما :(");
         }
     }

                    });
                }
                if (successDeleting == true)
                {
                    successDeleting = false;
                    alertify.success('تم الحذف بنجاح :)')
                }
                $scope.Unitsstobeodeleted = [];
                $(".checkedrow").attr("checked", false);
            }
//end
       


     //   console.log();
        alertify.set({ delay: 1000 });

        // read action
        $scope.ReUnits = [];
        $scope.read = function () {
            $.ajax({
                url: "http://localhost:1065/Unit/read",
                type: "POST"
                , success: function (data) {

                    $scope.ReUnits = data;
                    $scope.$apply();
                    storedarray = $scope.ReUnits;
                }

            });
        }
        if (storedarray.length == 0) { $scope.read(); } else { $scope.ReUnits = storedarray;}
        //end






        //to make click on row take id of unit and go to forms page
        //$('tr').click(function () {
        //    window.location = $(this).find('a').attr('href');
        //}).hover(function () {
        //    $(this).toggleClass('hover');
        //});
        //end

        //get project names and IDs 
        $scope.ReProjects = [];
        $scope.getProjects = function () {

            $.ajax({
                url: '/Unit/getprojectNameID'
                , type: "POST"
               , success: function (data) {
                   $scope.ReProjects = data;
                   $scope.$apply();
               }
            })
        }
        $scope.getProjects();
        //end


        //get building names and IDs 
        $scope.ReBuildings = [];
        $scope.getBuildings = function () {

            $.ajax({
                url: '/Unit/getbuildingNameID'
                , type: "POST"
               , success: function (data) {
                   $scope.ReBuildings = data;
                   $scope.$apply();
                  // console.log(data);
               }
            })
        }
        $scope.getBuildings();
        //end



        //testing create action and update 
        $scope.create = function (object) {
            //for sending
            var datay = {

                ModelNO: object.ModelNO,
                ModelName: object.ModelName,
                UnitNO: object.UnitNO,
                FloorNO: object.FloorNO,
                NOtes: object.NOtes,

                ReBuilding: { BuildingID: object.selectedBuilding }
            };

            if ($routeParams.UnitsID == -1) {
                $.ajax({
                    url: "/Unit/create",
                    type: "POST",
                    data: $.toDictionary(datay)
                    , success: function (data) {
                        if (data != false) {
                            alertify.success("تم الإضافه بنجاح :)");
                            $scope.form = [];
                            data.ReProject = $scope.searcharray($scope.ReProjects, 'ProjectID', object.selectedProject);
                            $scope.ReUnits.push(data);
                            console.log(data);
                            $scope.$apply();
                            storedarray = $scope.ReUnits;
                        }
                        else {
                            alertify.error("حدث خطأ :(");
                        }
                    }
                });
            }
            else {
                
                $scope.update(object);
            }
        }
        //end


        //testing delete action 
        $scope.delete = function () {


            $.ajax({
                url: "/Unit/delete",
                type: "POST",
                 data: {UnitsID:$routeParams.UnitsID}
            , success: function (data) {
               // console.log(data);
                if (data ==true) {

                    alertify.success("تم الحذف بنجاح :)");
                    $scope.form = [];
                    $scope.ReUnits.splice($scope.ReUnits.indexOf($scope.searcharray($scope.ReUnits, 'UnitsID' ,$routeParams.UnitsID)),1);
                    $scope.$apply();
                    storedarray = $scope.ReUnits;
                    console.log("deleted id :" + $.grep($scope.ReUnits, function (e) { return e.UnitsID == $routeParams.UnitsID; }))
                }
                else {

                    alertify.error("حدث خطأ ما :(");
                }
            }

            });



        }
       
        //end
        //search through array by id
        $scope.searcharray=function(array,prop,key){
            for (var i = 0; i < array.length; i++) {
                if(array[i][prop]==key)
                {
                    return array[i];
                }
            }
        
        }
        //end


        //testing update action

        $scope.update = function (object) {
            console.log(object);
            //for sending
             var datay = {
                 UnitsID:object.UnitsID,
                 ModelNO: object.ModelNO,
                 ModelName: object.ModelName,
                 UnitNO: object.UnitNO,
                 FloorNO: object.FloorNO,
                 NOtes: object.NOtes,

                 ReBuilding: { BuildingID: object.selectedBuilding }
             };
             console.log(object);
            //for saving in js 
             var mydatay = {
                 UnitsID: object.UnitsID,
                 ModelNO: object.ModelNO,
                 ModelName: object.ModelName,
                 UnitNO: object.UnitNO,
                 FloorNO: object.FloorNO,
                 NOtes: object.NOtes,

                 ReBuilding: { BuildingID: object.selectedBuilding,Name:$scope.searcharray($scope.ReBuildings,'BuildingID',object.selectedBuilding).Name },
                 ReProject: { ProjectID: object.selectedProject, Name: $scope.searcharray($scope.ReProjects, 'ProjectID', object.selectedProject).Name }
             };
            // object.ReBuilding.Name = $scope.searcharray($scope.ReBuildings, 'BuildingID', object.selectedBuilding).Name;
            // object.ReProject.Name = $scope.searcharray($scope.ReProjects, 'ProjectID', object.selected).Name;
            console.log(object)

            $.ajax({
             url: "/Unit/update",
             type: "POST",
             data: $.toDictionary(datay)
                , success: function (data) {

                //    console.log(datay);
                    if (data == true) {

                        alertify.success("تم التعديل بنجاح :)");
                        $scope.form = [];
                        $scope.ReUnits[$scope.ReUnits.indexOf($scope.searcharray($scope.ReUnits, 'UnitsID', object.UnitsID))] = mydatay;
                        
                        $scope.$apply();
                        console.log($scope.ReUnits[$scope.ReUnits.indexOf($scope.searcharray($scope.ReUnits, 'UnitsID', object.UnitsID))]);
                        console.log(mydatay);
                        storedarray = $scope.ReUnits;
                    }
                    else if (data == 'index') {
                        alertify.log("يوجد سجل آخر بنفس العمارة و الدور و الوحدة ", "error", 2500);
                       // alertify.error("يوجد سجل آخر بنفس العمارة و الدور و الوحدة ");
                    }
                    else {
                        alertify.error("حدث خطأ ما :(");
                    }


                }

             });
        }
        //end



        // to make details 
    //    console.log($routeParams.UnitsID);
        $scope.$watch('ReUnits', function () {
           // alertify.success($scope.ReUnits.length);
            if (($routeParams.UnitsID != -1) && ($routeParams.UnitsID != null)) {
                //console.log("true");
                //console.log($scope.ReUnits.length);

                for (var i = 0; i < $scope.ReUnits.length; i++) {

                    if ($scope.ReUnits[i].UnitsID == $routeParams.UnitsID) {
                        //  console.log("Iam in")
                        $scope.form = $scope.ReUnits[i];
                        //  console.log($scope.form);
                        $scope.form.selectedProject = $scope.ReUnits[i].ReProject.ProjectID;
                        $scope.form.selectedBuilding = $scope.ReUnits[i].ReBuilding.BuildingID;
                        console.log("form object")
                        console.log($scope.form);
                        console.log("reunits object")
                        console.log($scope.ReUnits[i]);
                        //$('.projectselect').val($scope.ReUnits[i].ReProject.ProjectID);
                        //$('.buildingselect').val($scope.ReUnits[i].ReBuilding.BuildingID);
                        //$(function () { 
                        //$('.projectselect').val($scope.ReUnits[i].ReProject.ProjectID);
                        //$('.buildingselect').val($scope.ReUnits[i].ReBuilding.BuildingID);
                        //})
                        //   console.log($scope.ReUnits[i].ReProject.ProjectID + "  BuildingID" + $scope.ReUnits[i].ReBuilding.BuildingID)

                        $scope.$apply();
                    }


                    ;
                }
            }
            else if ($routeParams.UnitsID == -1) {
               //$scope.$watch(
               //     $('option[value="?"]').text('أختر من');
              
               // console.log('$');
               // })
            }
        });
        //  end


     


        //managing table pagingation and search :)



        $scope.currentPage = 1; //current page
        $scope.maxSize = 5; //pagination max size
        $scope.entryLimit = 10; //max rows for data table

        /* init pagination with $scope.list */
        $scope.noOfPages = Math.ceil($scope.ReUnits.length / $scope.entryLimit);
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };
        $scope.$watch('ReUnits+entryLimit',function () { $scope.noOfPages = Math.ceil($scope.ReUnits.length / $scope.entryLimit) });
        $scope.filter = function () {
            $timeout(function () { //wait for 'filtered' to be changed
                /* change pagination with $scope.filtered */
                $scope.noOfPages = Math.ceil($scope.filtered.length / $scope.entryLimit);
            }, 10);
        };

        //end




        // view loading until route page is loaded :) :) :) 
        $scope.isViewLoading = false;
        //imp how to make event liseners :) :) 
        $scope.$on('$routeChangeStart', function () {
            $scope.isViewLoading = true;
        });
        $scope.$on('$routeChangeSuccess', function () {
            $scope.isViewLoading = false;
        });
        //
        //end of  view loading until route page is loaded :) :) :) 







    })}


//end code for table 
