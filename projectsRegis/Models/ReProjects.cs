//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace projectsRegis.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReProjects
    {
        public ReProjects()
        {
            this.ReBuilding = new HashSet<ReBuilding>();
        }
    
        public int ProjectID { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
    
        public virtual ICollection<ReBuilding> ReBuilding { get; set; }
    }
}
