﻿using projectsRegis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projectsRegis.Controllers
{
    public class HomeController : Controller
    {//from CRUD projects
        public ActionResult Index()
        {


            return View();
        }
        //from CRUD Buildings 
        //Home/Building
        public ActionResult Building()
        {


            return View();
        }
        //from CRUD Units
        //Home/Unit
        public ActionResult Unit()
        {


            return View();
        }

        //from CRUD Customers
        //Home/Customer
        public ActionResult Customer()
        {


            return View();
        }
        public ActionResult Report()
        {


            return View();
        }
        //to return the from units ng-view the form page
        public ActionResult unitsform()
        {
            return View();
        }

        //to return the from units ng-view the table page
        public ActionResult unitstable()
        {
            return View();
        }

        public JsonResult allprojects()
        {

            BuildingEntities mydb = new BuildingEntities();

            return Json(mydb.ReProjects.Select(a => new { id = a.ProjectID, name = a.Name, notes = a.Notes }), JsonRequestBehavior.AllowGet);
        }


        public string delete(int id)
        {
            try
            {
                BuildingEntities mydb = new BuildingEntities();
                mydb.ReProjects.Remove(mydb.ReProjects.Find(id));
                mydb.SaveChanges();
                return "deleted";
            }

            catch (Exception e)
            {
                //if (e.StackTrace.Contains("REFERENCE constraint"))
                //{
                return "childparenterror";
                //}
                //else
                //{
                //    return "other";
                //}
            }



        }

        public string add(string projectname, string projectnotes, int projectid = -1)
        {

            BuildingEntities mydb = new BuildingEntities();
            var myproject = mydb.ReProjects.Find(projectid);
            if (myproject == null)
            {
                ReProjects mynew = new ReProjects { Name = projectname, Notes = projectnotes };

                mydb.ReProjects.Add(mynew);
                mydb.SaveChanges();
                return "added";
            }
            else
            {
                myproject.Name = projectname;
                myproject.Notes = projectnotes;
                mydb.SaveChanges();
                return "edited";
            }

        }
       
        // just trying to get solution for circular reference but it doesnot go as supposed
        public JsonResult get()
        { 
        
            BuildingEntities mydb=new BuildingEntities();

            var result = from building in mydb.ReBuilding
                         join project in mydb.ReProjects
                         on building.ProjectID equals project.ProjectID
                         select building;

        return Json(result,JsonRequestBehavior.AllowGet);
        }
        
    }




    //public  class myReBuilding
    //{
        
    //    public int BuildingID { get; set; }
    //  //  public int ProjectID { get; set; }
    //    public string Name { get; set; }
    //    public string Notes { get; set; }

    //    public  myReProjects ReProjects { get; set; }
    //  //  public virtual ICollection<ReUnits> ReUnits { get; set; }
    //}

    //public class myReProjects
    //{

    //    public int ProjectID { get; set; }
    //    public string Name { get; set; }
    //    public string Notes { get; set; }

    //    public virtual ICollection<ReBuilding> ReBuilding { get; set; }
    //}
 
}
