﻿using projectsRegis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projectsRegis.Controllers
{
    public class UnitController : Controller
    {

        //to get all records to put it on our grid 
        public JsonResult read()
        {
            BuildingEntities mydb = new BuildingEntities();
            var units=mydb.ReUnits;
           // var result = units.Select(a => new { a, ReBuilding = new { a.ReBuilding.BuildingID }, ReProjects = new { a.ReBuilding.ReProjects.ProjectID } });
            var result = from a in units
                         select new {a.ModelName,a.ModelNO,a.UnitNO,a.FloorNO,a.NOtes,a.Is_reserved,a.UnitsID,ReBuilding=new{ a.ReBuilding.BuildingID,a.ReBuilding.Name},ReProject=new{ a.ReBuilding.ReProjects.ProjectID,a.ReBuilding.ReProjects.Name} };
            return Json(result,JsonRequestBehavior.AllowGet);
        }

        //to update (edit ) a unit 
        public JsonResult update(UNIT NewUnit)
        {
            BuildingEntities mydb = new BuildingEntities();
            var units = mydb.ReUnits;
            try
            {
                ReUnits newFilledUnit = units.Find(NewUnit.UnitsID);
                newFilledUnit.ModelNO = NewUnit.ModelNO;
                newFilledUnit.ModelName = NewUnit.ModelName;
                newFilledUnit.UnitNO = NewUnit.UnitNO;
                newFilledUnit.FloorNO = NewUnit.FloorNO;
                newFilledUnit.NOtes = NewUnit.NOtes;
                newFilledUnit.ReBuilding = mydb.ReBuilding.Find(NewUnit.ReBuilding.BuildingID);

              
                mydb.SaveChanges();

                //newFilledUnit.ReBuilding.ReUnits = null;
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                if (e.HResult == -2146233087)
                return Json("index", JsonRequestBehavior.AllowGet);

                return Json(false, JsonRequestBehavior.AllowGet);
            }
         


          ;
        }

        //to delete a unit 
        public JsonResult delete(int UnitsID)
        {
            BuildingEntities mydb = new BuildingEntities();
            var units = mydb.ReUnits;
            try
            {
                units.Remove(units.Find(UnitsID));
                mydb.SaveChanges(); 
                
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch(Exception )
            {
                return Json(false , JsonRequestBehavior.AllowGet);
            }

            
        }

        //to create new row (unit)
       
        public JsonResult create(UNIT NewUnit)
        {

            BuildingEntities mydb = new BuildingEntities();
            var units = mydb.ReUnits;
            try
            {
                ReUnits newFilledUnit = new ReUnits();
                newFilledUnit.ModelNO = NewUnit.ModelNO;
                newFilledUnit.ModelName = NewUnit.ModelName;
                newFilledUnit.UnitNO = NewUnit.UnitNO;
                newFilledUnit.FloorNO = NewUnit.FloorNO;
                newFilledUnit.NOtes = NewUnit.NOtes;
                newFilledUnit.ReBuilding=mydb.ReBuilding.Find(NewUnit.ReBuilding.BuildingID);
                
                units.Add(newFilledUnit);
                mydb.SaveChanges();

                newFilledUnit.ReBuilding.ReUnits = null;
                return Json(newFilledUnit, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
         
        }




        public object getProjectNameID()
        {
            BuildingEntities mydb = new BuildingEntities();
            var result = from i in mydb.ReProjects
                         select new { Name = i.Name, ProjectID = i.ProjectID };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //end


        //get list with all  names and ids
        public object getbuildingNameID()
        {
            BuildingEntities mydb = new BuildingEntities();
            var result = from i in mydb.ReBuilding
                         select new { Name = i.Name,BuildingID=i.BuildingID, ProjectID = i.ProjectID };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //end
     

    }


































    public class UNIT
    {
            public int UnitsID { get; set; }
        public int BuildingID { get; set; }
        public string ModelName { get; set; }
        public string ModelNO { get; set; }
        public string FloorNO { get; set; }
        public string UnitNO { get; set; }
        public bool Is_reserved { get; set; }
        public Nullable<byte> IS_PayedAll { get; set; }
        public string NOtes { get; set; }
    
        public virtual ReBuilding ReBuilding { get; set; }
        public virtual ICollection<ReCustBuilding> ReCustBuilding { get; set; }
    }
}
