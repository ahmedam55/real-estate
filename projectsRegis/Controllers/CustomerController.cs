﻿using Newtonsoft.Json;
using projectsRegis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projectsRegis.Controllers
{
    public class CustomerController : Controller
    {
        //
        // GET: /Customer/
        public JsonDotNetResult create(Customers customer)
        {

            BuildingEntities mydb = new BuildingEntities();
            mydb.Configuration.LazyLoadingEnabled = true;
            ReCustomers newCust = new ReCustomers();
            newCust.Name = customer.Name;
            newCust.PersonalNO = customer.PersonalNO;
            newCust.Address = customer.Address;
            newCust.TelNO = customer.TelNO;
            newCust.MobileNO = customer.MobileNO;
            newCust.Notes = customer.Notes;
            newCust.ReCustBuilding = customer.ReCustBuilding;
            newCust.ReCustInstallment = customer.ReCustInstallment;

            mydb.ReCustomers.Add(newCust);
            //mydb.SaveChanges();
            //try
            //{

            //for (int i = 0; i < newCust.ReCustBuilding.Count; i++)
            //{
            //    mydb.ReUnits.Find(newCust.ReCustBuilding.ElementAt(i).UnitsID).Is_reserved = true;
            //    //newCust.ReCustBuilding.ElementAt(i).ReUnits.Is_reserved = true;
            //}
           

           
            //}
            //catch (Exception)
            //{

              

            //}
            mydb.SaveChanges();
            reserve();
            return new JsonDotNetResult(newCust.CustomerID);
        }



        //to reserve the commited units and rest is unreserved 
        private void reserve() {

            BuildingEntities mydb = new BuildingEntities();
          //  mydb.Configuration.LazyLoadingEnabled = true;
            var unit = (from unity in mydb.ReUnits                      
                         select unity.UnitsID).ToList();


            var commited = (from custBuild in mydb.ReCustBuilding
                           select custBuild.UnitsID).ToList();

            var uncommited = (from y in unit
                             where !commited.Contains(y)
                             select y).ToList();

            for (int i = 0; i < commited.Count; i++)
            {
                mydb.ReUnits.Find(commited[i]).Is_reserved = true;
            }

            for (int i = 0; i < commited.Count; i++)
            {
                mydb.ReUnits.Find(uncommited[i]).Is_reserved = false;
            }
            mydb.SaveChanges();
           // var commited = unit.Intersect(building);


            //for (int i = 0; i < result.Count(); i++)
            //{
            //    result.ElementAt(i).Is_reserved = false;
            //}



            //var result2 = from unit in mydb.ReUnits
            //              where unit.ReCustBuilding.ElementAt(0) != null
            //             select unit;

            //for (int i = 0; i < result2.Count(); i++)
            //{
            //    result2.ElementAt(i).Is_reserved = true;
            //}


            //var commited = mydb.ReUnits.Where(d => d.ReCustBuilding.Any(a=>a.UnitsID!=null));

            //for (int i = 0; i < commited.Count(); i++)
            //{
            //    commited.ElementAt(i).Is_reserved = true;
            //}

            //var uncommited = mydb.ReUnits.Where(d => d.ReCustBuilding.Any(a => a.UnitsID == null));

            //for (int i = 0; i < uncommited.Count(); i++)
            //{
            //    uncommited.ElementAt(i).Is_reserved = false;
            //}

            mydb.SaveChanges();        
        }




        public JsonResult update(Customers customer)
        {

            BuildingEntities mydb = new BuildingEntities();
            mydb.Configuration.LazyLoadingEnabled = true;
            ReCustomers newCust = mydb.ReCustomers.Find(customer.CustomerID);
            newCust.Name = customer.Name;
            newCust.PersonalNO = customer.PersonalNO;
            newCust.Address = customer.Address;
            newCust.TelNO = customer.TelNO;
            newCust.MobileNO = customer.MobileNO;
            newCust.Notes = customer.Notes;

            newCust.ReCustBuilding.ToList().ForEach(a=>mydb.ReCustBuilding.Remove(a));
            newCust.ReCustInstallment.ToList().ForEach(a => mydb.ReCustInstallment.Remove(a));


            newCust.ReCustBuilding = customer.ReCustBuilding;
            newCust.ReCustInstallment = customer.ReCustInstallment;

            //try
            //{

            //    for (int i = 0; i < newCust.ReCustBuilding.Count; i++)
            //    {
            //        mydb.ReUnits.Find(newCust.ReCustBuilding.ElementAt(i).UnitsID).Is_reserved = true;
            //    }



            //}
            //catch (Exception)
            //{



            //}



           // newCust.ReCustInstallment = customer.ReCustInstallment;


            //var element=newCust.ReCustBuilding.ToList();
            //for (int i = 0; i < element.Count; i++)
            //{
            //    mydb.ReCustBuilding.Remove(mydb.ReCustBuilding.Single(a=>a.CustomerID==111));
                
            //}    
            //var oldBuildings = newCust.ReCustBuilding;
           // newCust.ReCustBuilding;
            //for (int i = 0; i < newCust.ReCustBuilding.Count; i++)
            //{
            //   // newCust.ReCustBuilding.Remove(newCust.ReCustBuilding.ElementAt(i));
            //    mydb.ReCustBuilding.Remove(mydb.ReCustBuilding.Find(107));
            //}
            //newCust.ReCustInstallment.Clear();
            mydb.SaveChanges();
            reserve();
           // mydb.SaveChanges();
            // mydb.ReCustomers.Add(newCust);
            //try

            //to make old one(not associated with customer  isreserved=false)
            //and new one is reserved =true;


            //////var intersection = oldBuildings.Intersect(newCust.ReCustBuilding);

            //////var oldArray = oldBuildings.Except(intersection);
            //////var newArray = newCust.ReCustBuilding.Except(intersection);

            //////for (int i = 0; i < oldArray.Count(); i++)
            //////{
            //////    oldArray.ElementAt(i).ReUnits.Is_reserved = false;
            //////}

            //////for (int i = 0; i < newArray.Count(); i++)
            //////{
            //////    newArray.ElementAt(i).ReUnits.Is_reserved = true;
            //////}

            //{
          //  mydb.SaveChanges();

            return Json(true);
            //}
            //catch (Exception) {

            //    return Json(false);

            //}
        }



        public JsonResult delete(int id)
        {

            BuildingEntities mydb = new BuildingEntities();
            ReCustomers newCust = mydb.ReCustomers.Find(id);
           

            mydb.ReCustomers.Remove(newCust);
            mydb.SaveChanges();
            reserve();
            return Json(true);

        }






        public JsonDotNetResult read(int id)
        {
            
            BuildingEntities mydb = new BuildingEntities();    
           // mydb.Configuration.LazyLoadingEnabled = true;
            //mydb.Configuration.ProxyCreationEnabled = false;
          
            //var newCust = from cust in mydb.ReCustomers
            //                  .Include("ReCustBuilding")   
            //                  .Include("ReCustInstallment")
            //              select cust;



            var newCust = (from cust in mydb.ReCustomers    
                          where cust.CustomerID==id
                         select new
                          {
                              cust.CustomerID,
                              cust.Name,
                              cust.PersonalNO,
                              cust.Address,
                              cust.TelNO,
                              cust.MobileNO,
                              cust.Notes,
                              ReCustBuilding = cust.ReCustBuilding.Select(a => new { a.CustBuildingID, a.CustomerID, a.UnitsID, a.PayedTYpe, a.Price, a.NoInstallment, a.FirstInstallment,a.PeriodInstallment, a.FirstDateInstallment, a.payedAmount, a.DateContract, ReUnits = new {a.ReUnits.BuildingID,a.ReUnits.ReBuilding.ProjectID, a.ReUnits.UnitNO, a.ReUnits.UnitsID,a.ReUnits.FloorNO,a.ReUnits.ModelName,a.ReUnits.ModelNO, a.ReUnits.ReBuilding.Name } }),
                              ReCustInstallment = cust.ReCustInstallment.Select(a => new { a.InstallmentID, a.CustomerID, a.UnitsID, a.DateInstallment, a.value, a.datePayed, a.Payed, a.InstallmentNO })
                          }).FirstOrDefault();

            return new JsonDotNetResult(newCust);
           // return Json(newCust);

        }

        //get clients Names :)
        public JsonResult getNames()
        {
            BuildingEntities mydb = new BuildingEntities();
            return Json(mydb.ReCustomers.Select(a => new { a.Name, a.CustomerID }));
        }


        //get data of units 
        public JsonDotNetResult getNamesUnits()
        {
            BuildingEntities mydb = new BuildingEntities();
            var result = mydb.ReUnits.Where(a => a.Is_reserved == false).Select(a => new {a.BuildingID, a.UnitsID, a.UnitNO, a.ModelNO, a.ModelName, a.FloorNO });
            return new JsonDotNetResult(result);
        }


        ////get data of units 
        //public JsonDotNetResult getNamesUnits()
        //{
        //    BuildingEntities mydb = new BuildingEntities();
        //    var result = mydb.ReUnits.Where(a => a.Is_reserved == false).Select(a => new { a.BuildingID, a.UnitsID, a.UnitNO, a.ModelNO, a.ModelName, a.FloorNO });
        //    return new JsonDotNetResult(result);
        //}



    }








    public class Customers
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string PersonalNO { get; set; }
        public string Address { get; set; }
        public string TelNO { get; set; }
        public string MobileNO { get; set; }
        public string Notes { get; set; }

        public virtual ICollection<ReCustBuilding> ReCustBuilding { get; set; }
        public virtual ICollection<ReCustInstallment> ReCustInstallment { get; set; }
        public virtual ICollection<RePayments> RePayments { get; set; }
    }

    // aclass to customize JSON converter class to be not like that ( "Name":"" ) ==> (Name:'fdsfsdf')
    public class JsonDotNetResult : ActionResult
    {
        private object _obj { get; set; }
        public JsonDotNetResult(object obj)
        {
            _obj = obj;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.AddHeader("content-type", "application/json");
            context.HttpContext.Response.Write(JsonConvert.SerializeObject(_obj, Formatting.None,
    new JsonSerializerSettings
    {
        ReferenceLoopHandling = ReferenceLoopHandling.Ignore


    }));
        }
    }







}
