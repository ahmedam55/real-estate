﻿using projectsRegis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projectsRegis.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        public ActionResult Index()
        {
            return View();
        }



        //old old old old old old old 
        public JsonResult getAllProjectsData (){

            BuildingEntities mydb = new BuildingEntities();
            var cash = (from cashy in mydb.ReUnits
                       where ((from c in cashy.ReCustBuilding where (c.PayedTYpe==1&&c.ReUnits.Is_reserved==true) select c.CustBuildingID).Count()!=0)
                        select cashy).Count();
            var installment = (from cashy in mydb.ReUnits
                        where ((from c in cashy.ReCustBuilding where (c.PayedTYpe == 2 && c.ReUnits.Is_reserved == true) select c.CustBuildingID).Count() != 0)
                        select cashy).Count();
            var notSold = (from cashy in mydb.ReUnits
                               where ((from c in cashy.ReCustBuilding where ( c.ReUnits.Is_reserved == false) select c.CustBuildingID).Count() != 0)
                               select cashy).Count();
        
            var result = new
            {
                notSold = notSold
                ,
                installment = installment,
                cash =cash
            };
            return Json(result,JsonRequestBehavior.AllowGet);
        
        }


        //old old old old old old old 
        public JsonDotNetResult getAllProjectsDataByName()
        {

            BuildingEntities mydb = new BuildingEntities();
           // mydb.Configuration.LazyLoadingEnabled = true;
            var cash = from cashy in mydb.ReUnits

                       where ((from c in cashy.ReCustBuilding where (c.PayedTYpe == 1 && c.ReUnits.Is_reserved == true) select c.CustBuildingID).Count() != 0)
                       group cashy by cashy.ReBuilding.ReProjects.ProjectID into r
                       select r;
            var installment = (from cashy in mydb.ReUnits
                               where ((from c in cashy.ReCustBuilding where (c.PayedTYpe == 2 && c.ReUnits.Is_reserved == true) select c.CustBuildingID).Count() != 0)
                               select cashy).Count();
            var notSold = (from cashy in mydb.ReUnits
                           where ((from c in cashy.ReCustBuilding where (c.ReUnits.Is_reserved == false) select c.CustBuildingID).Count() != 0)
                           select cashy).Count();

            var result = new
            {
                notSold = notSold
                ,
                installment = installment,
                cash = cash
            };
            return new JsonDotNetResult(cash);

        }




        //the working method
        //gives me all data of the company 
        //and I shall analyize it ther on the page 
        //by using pivot table I guess :)
        [OutputCache(Duration=360)]
        public JsonDotNetResult BIG() {


            BuildingEntities mydb = new BuildingEntities();


            var BIG = from project in mydb.ReProjects

                      //let xyz=project.
                      //let unitss = mydb.ReProjects.Find(xyz).ReBuilding.Select(x => x.ReUnits.Select(a => new
                      //{
                      //    a.FloorNO,
                      //    a.Is_reserved,
                      //    a.ModelName,
                      //    a.ModelNO,
                      //    a.NOtes,
                      //    a.UnitNO,
                      //    customer = a.ReCustBuilding.Select(d => new
                      //    {
                      //        d.DateContract,
                      //        d.FirstDateInstallment,
                      //        d.FirstInstallment,
                      //        d.NoInstallment,
                      //        d.PayedTYpe,
                      //        d.PeriodInstallment,
                      //        d.Price,
                      //        d.ReCustomers.Address,
                      //        d.ReCustomers.MobileNO,
                      //        d.ReCustomers.Name,
                      //        d.ReCustomers.Notes,
                      //        d.ReCustomers.PersonalNO,
                      //        d.ReCustomers.TelNO,

                      //        installment = d.ReCustomers.ReCustInstallment.Select(e => new { e.DateInstallment, e.InstallmentNO, e.Payed, e.value })
                      //    })
                      //}))
                      select new
                      {
                          project.Name,
                          project.Notes,
                          building = project.ReBuilding.Select(a => new
                          {
                              a.Name,
                              a.Notes,
                              units = a.ReUnits.Select(e => new
                              {
                                  e.FloorNO,
                                  e.Is_reserved,
                                  e.ModelName,
                                  e.ModelNO,
                                  e.NOtes,
                                  e.UnitNO,
                                  customer = e.ReCustBuilding.Select(d => new
                                  {
                                      d.DateContract,
                                      d.FirstDateInstallment,
                                      d.FirstInstallment,
                                      d.NoInstallment,
                                      d.PayedTYpe,
                                      d.PeriodInstallment,
                                      d.Price,
                                      d.ReCustomers.Address,
                                      d.ReCustomers.MobileNO,
                                      d.ReCustomers.Name,
                                      d.ReCustomers.Notes,
                                      d.ReCustomers.PersonalNO,
                                      d.ReCustomers.TelNO,

                                      installment = d.ReCustomers.ReCustInstallment.Select(t => new { t.DateInstallment, t.InstallmentNO, t.Payed, t.value })
                                  })
                              })
                          })

                      };


            return new JsonDotNetResult(BIG);
        }


        //private JsonDotNetResult help(int xyz) {
        //    BuildingEntities mydb = new BuildingEntities();


        //  var unitss = mydb.ReProjects.Find(xyz).ReBuilding.Select(x => x.ReUnits.Select(a => new
        //              {
        //                  a.FloorNO,
        //                  a.Is_reserved,
        //                  a.ModelName,
        //                  a.ModelNO,
        //                  a.NOtes,
        //                  a.UnitNO,
        //                  customer = a.ReCustBuilding.Select(d => new
        //                  {
        //                      d.DateContract,
        //                      d.FirstDateInstallment,
        //                      d.FirstInstallment,
        //                      d.NoInstallment,
        //                      d.PayedTYpe,
        //                      d.PeriodInstallment,
        //                      d.Price,
        //                      d.ReCustomers.Address,
        //                      d.ReCustomers.MobileNO,
        //                      d.ReCustomers.Name,
        //                      d.ReCustomers.Notes,
        //                      d.ReCustomers.PersonalNO,
        //                      d.ReCustomers.TelNO,

        //                      installment = d.ReCustomers.ReCustInstallment.Select(e => new { e.DateInstallment, e.InstallmentNO, e.Payed, e.value })
        //                  })
        //              }));

        //    return new JsonDotNetResult(true);
        //}

    }
}
