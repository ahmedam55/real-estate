﻿using projectsRegis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projectsRegis.Controllers
{
    public class BuildingController : Controller
    {
        //
        // GET: /Building/

      

        /// <summary>
        /// ///////////////// building page code
        /// </summary>
        /// <returns></returns>
        public JsonResult getbuildingdata()
        {
            BuildingEntities mydb = new BuildingEntities();
            // var result1 = from c in mydb.ReBuilding select  new { c.BuildingID, projectName = c.ReProjects.Name, buildingName = c.Name, c.Notes };
            var result = from row in mydb.ReBuilding
                         let pro = row.ReProjects
                         select new { Name = row.Name, Notes = row.Notes, BuildingID = row.BuildingID, ReProjects = new { Name = pro.Name, ProjectID = pro.ProjectID } };



            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public string deletebuilding(int id)
        {
            try
            {
                BuildingEntities mydb = new BuildingEntities();
                mydb.ReBuilding.Remove(mydb.ReBuilding.Find(id));
                mydb.SaveChanges();
                return "deleted";
            }
            catch (Exception)
            {
                return "error";
            }
        }

        // http://localhost:1065/Home/addbuilding?buildingname=q&buildingnotes=qwerty&projectid=1023&buildingid=22
        public JsonResult addbuilding(string buildingname, string buildingnotes, int projectid, int buildingid = -1)
        {

            BuildingEntities mydb = new BuildingEntities();
            var mybuilding = mydb.ReBuilding.Find(buildingid);
            if (mybuilding == null)
            {
                ReBuilding mynew = new ReBuilding { Name = buildingname, Notes = buildingnotes, ReProjects = mydb.ReProjects.Find(projectid) };

                mydb.ReBuilding.Add(mynew);
                mydb.SaveChanges();
                var result = new { mynew, type = "added" };
                result.mynew.ReProjects.ReBuilding = null;
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                mybuilding.Name = buildingname;
                mybuilding.Notes = buildingnotes;
                mybuilding.ReProjects = mydb.ReProjects.Find(projectid);
                mydb.SaveChanges();
                var result = new { mynew = mybuilding, type = "edited" };
                result.mynew.ReProjects.ReBuilding = null;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }


        //get list with all projects names and ids
        public object getProjectNameID()
        {
            BuildingEntities mydb = new BuildingEntities();
            var result = from i in mydb.ReProjects
                         select new { name = i.Name, id = i.ProjectID };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        //end

    }
}
